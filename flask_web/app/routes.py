import flask
from app import app
from app.backend.mongo_class import Mongod

@app.route('/')
@app.route('/index')
def index():
    stats = [{'name': 'Flask Version','body': flask.__version__},
            {'name': 'Authors','body': 'Mario Alejandro Manzano, Jesus Angel Rodriguez Martinez'}]
    return flask.render_template('index.html', stats=stats)

@app.route('/users')
def users():
    conn = Mongod('mario','12345')
    users = conn.get_users()
    conn.exit()
    return flask.render_template('users.html', users=users)

@app.route('/records')
def records():
    conn = Mongod('mario','12345')
    records = conn.get_records()
    conn.exit()
    return flask.render_template('records.html', records=records)