import pymongo

class Mongod:
    def __init__(self, usr, pwd):
        self.usr = usr
        self.pwd = pwd
        self.conn = pymongo.MongoClient('localhost', 27017)
        self.db = self.conn["fsc"]
    
    def get_users(self):
        col = self.db["users"]
        users = []
        for u in col.find():
            users.append(u)
        return users

    def backup_users(self, users):
        col = self.db["backup_users"]
        for id, data in users.items():
            if col.find_one({"_id":id}) is None:
                col.insert_one({"_id":id,"user":data['user'], "fingers":data['fingers']})
            else:
                col.update_one({"_id":id}, {"$set": {"user":data['user'], "fingers":data['fingers']}})
    
    def restore_users(self):
        col = self.db["backup_users"]
        users = []
        cur = col.find({})
        for doc in cur:
            users.append(doc)
        return users
    
    def store_users(self, users):
        col = self.db["users"]
        data = []
        for key, value in users.items():
            data.append({"_id":key, "data":value})
        col.insert_many(data)

    def store_records(self, records):
        col = self.db["records"]
        for i in records:
            for id, d in i.items():
                dt = d['date'].split('-')
                if col.find_one({"_id":id}) is None:
                    col.insert_one({"_id":id})
                col.update_one({"_id":id},{"$addToSet": {"{}.{}.{}".format(dt[0],dt[1],dt[2]):
                        {"time":d['time'],"status":str(d['punch'])}}})
    
    def get_records(self):
        col = self.db["records"]
        records = []
        for r in col.find():
            records.append(list(r.items()))
        return records

    def get_bydate(self, d):
        col = self.db["records"]
        records = []
        for r in col.find({"{}.{}.{}".format(d[0],d[1],d[2]): {"$exists":1}}):
            records.append(r)
        return records

    def get_byuser(self, id):
        col = self.db["records"]
        return col.find_one({"_id":id})

    def exit(self):
        self.conn.close()


if __name__ == "__main__":
    pass