import datetime
import sys
from ztk_class import Ztk
from mongo_class import Mongod
from excel_class import Excel

def backup():
    ztk = Ztk()
    users = ztk.get_users()
    ztk.exit()
    del ztk
    mdb = Mongod("mario", "12345")
    mdb.backup_users(users)
    mdb.exit()
    del mdb

def restore():
    mdb = Mongod("mario", "12345")
    users = mdb.restore_users()
    mdb.exit()
    del mdb
    ztk = Ztk()
    ztk.upload_users(users)
    ztk.exit()
    del ztk

def update():
    excel = Excel()
    excel.update()
    users = excel.load()
    del excel
    mdb = Mongod("mario", "12345")
    mdb.store_users(users)
    mdb.exit()
    del mdb

if __name__ == "__main__":
    log = open('logs/backup_logs.txt', 'a')
    log_txt = ""
    try:
        if "-backup" in sys.argv:
            log_txt += "Backing up Users and Biometrics ...\n"
            backup()
            log_txt += "Backed up and Stored in MongoDB ...\n"
        elif "-restore" in sys.argv:
            log_txt += "Restoring Users and Biometrics ...\n"
            restore()
            log_txt += "Machine has been restored ...\n"
        elif "-update" in sys.argv:
            log_txt += "Downloadig user DB from excel ...\n"
            update()
            log_txt += "Loaded DB in Mongo DB ...\n"
        else:
            log_txt += "Error: add  -backup / -restore or -update to the end of the terminal ...\n"
            raise Exception(log_txt)
        log_txt += "Finished ...\n"
    except Exception as e:
        error = open('logs/backup_errors.txt', 'a')
        error.write("Date: {} \nProcess Error : {}\n\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),e))
        error.close()
    finally:
        log.write("Date: {} \nProcess Report : {}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), log_txt))
        log.close()
        print(log_txt)