from openpyxl import load_workbook
import json

class Excel:
    def update(self):
        wb = load_workbook('db_files/personal_fsc.xlsx')
        ws = wb.active
        users = {}
        for row in ws.iter_rows(min_row=2, max_col=8, values_only=True):
            if row[0] != None:
                row = list(row)
                [row.remove(e) for e in row if e == None]
                users[row[0]] = {"nam":row[1],"lst":row[2],"dptn":row[3],"dpt":row[4],"gnd":row[5]}
        f = open('db_files/users.txt', 'w')
        f.write(json.dumps(users))
        f.close()
    
    def load(self):
        return json.loads(open('db_files/users.txt', 'r').read())